import cv2

cap = cv2.VideoCapture(0)
img_counter = 0
x = 0
img = 0
while True:
    img = cv2.imread("../static/img/image_{}.png".format(img_counter), 1)
    if img is None:
        break
    else:
        img_counter += 1
while True:
    ret, frame = cap.read()
    key = cv2.waitKey(1)
    # condition for take one photo
    if x == 0:
        img_name = "../static/img/image_{}.png".format(img_counter)
        cv2.imwrite(img_name, frame)
        print("{} written!".format(img_name))
        img_counter += 1
        x += 1
    else:
        break
cv2.waitKey(1000)
cv2.destroyAllWindows()
