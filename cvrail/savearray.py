import os
import cv2
import cv2.aruco as aruco

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters = aruco.DetectorParameters_create()
# number image
number = 0
# variables for cycles
y = 0
t = 0
i = 0
# array that stores the first values of each ordered array
xy = []
size = 0
path = "/home/simao/dev/cvrail/"
while number is not None:
    img = cv2.imread(path + "static/img/image_{}.png".format(number), 1)
    numbers = []
    if img is not None:
        corners, ids, rejectedImgPoints = aruco.detectMarkers(img, aruco_dict, parameters=parameters)
        out = aruco.drawDetectedMarkers(img, corners, ids)
        file = open(path + "tests/image_{}.txt".format(number), "w+")
        if number == 0:
            # Define how many squares there are initially
            size = len(corners)
        for t in range(0, len(corners)):
            # if it has fewer squares it is necessary to define the arrays again
            if len(corners) != size:
                lis = []
                xy = []
            # lis will store the first value of each array
            lis = corners[t]
            res = [array_1[0] for array_1 in lis]
            # will be saved in the res and added one by one to the numbers
            numbers.append(str(res))
        # xy will store the ordered values and then order the main array
        xy = sorted(numbers)
        number += 1
    else:
        break
    check = 'True'
    final = []
    p = 0
    if not corners:
        # condition for the case not detecting any square write something in the txt file
        file.write("%s" % corners)
        file.write("\n<---------->\n")
        file.close()
    else:
        while check:
            if len(corners) != size:
                array_2 = []
            array_2 = []
            # array_2 save the array by array
            array_2 = corners[p]
            # resc_2 will store the values of the first position of the array
            res_2 = [yu[0] for yu in array_2]
            # Condition that will make the ordering.
            # The cycle will run all the arrays of the corners, and if it finds what should be in first it adds it to a final array
            if xy[i] == str(res_2):
                final.append(corners[p])
                # Go to next position of xy to find the following array with the smallest x
                i += 1
                y += 1
            # condition to exit if you have already covered all the squares
            if y == len(corners):
                check = 'False'
                break
            p += 1
            # condition to avoid infinite cycles
            if p == len(corners):
                p = 0
        y = 0
        i = 0
        for d in range(0, len(final)):
            # save coordinates in txt files
            file.write("%s" % final[d])
            file.write("\n<---------->\n")
        file.close()
while number is not None:
    # remove a txt file if there are no matching images
    if os.path.exists(path + "tests/image_{}.txt".format(number)):
        os.remove(path + "tests/image_{}.txt".format(number))
        number += 1
    else:
        print("The file does not exist->", number)
        break
