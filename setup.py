from setuptools import setup


setup(
    name='CVrail',
    version='1.9.2',
    author='Simão Duarte',
    author_email='maravilha1sapo@hotmail.com',
    url='https://gitlab.com/simaoduarte/cvrail',
    description="Detects movement on railway slopes",
    long_description='Through pictures taken on the slopes with the Aruco squares it is possible to detect whether '
                     'there was movement on the slopes or not',
    classifier=[
        'Development Status :: 5 - Production',
        'License :: OSI Approved :: GNU License',
        'Programming Language :: Python :: 3.7',
    ],
)

