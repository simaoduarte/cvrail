import os
import cv2

img_counter = 0
count = 1
path = "/home/simao/dev/cvrail/"
while True:
    img = cv2.imread(path + "static/img/image_{}.png".format(img_counter), 1)
    if img is None:
        break
    else:
        img_counter += 1
while count < img_counter:
    os.remove(path + "static/img/image_{}.png".format(count))
    os.remove(path + "tests/image_{}.txt".format(count))
    count += 1
