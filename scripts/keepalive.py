import xmlrpc.client
import time
import datetime
import base64
import cv2

url = "http://localhost:8069"
db = 'v12_cvrail'
username = 'admin'
password = 'admin'

path = "/home/simao/dev/cvrail/"


def script_keep_alive():
    common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
    models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
    # logins
    uid = common.authenticate(db, username, password, {})
    img_counter = 0
    while True:
        z = cv2.imread(path + "static/img/image_{}.png".format(img_counter), 1)
        if z is None:
            img_counter -= 1
            break
        else:
            img_counter += 1
    with open(path + "static/img/image_{}.png".format(img_counter), "rb") as imageFile:
        ImageBase64 = base64.b64encode(imageFile.read())
        str = ImageBase64.decode('ascii')
    date_act = datetime.datetime.now()
    new_partner = {
        'name': "Viseu",
        'reason': "Keep Alive",
        'status': 1,
        'date': date_act,
        'notes': "Its a keep alive",
    }
    new_contact = models.execute_kw(db, uid, password, 'open.cvrail', 'create', [new_partner])
    new_image = {
        'n_image': 1,
        'photo': str,
        'image_id': new_contact,
    }
    new_image_id = models.execute_kw(db, uid, password, 'open.cvrail.image', 'create', [new_image])
    print("New created id is", new_contact)


script_keep_alive()
