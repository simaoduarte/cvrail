import xmlrpc.client
import time
import datetime
import base64
import os

url = "http://localhost:8069"
db = 'v12_cvrail'
username = 'admin'
password = 'admin'


def script(dir1, dir2, dir3, reason_for, num_txt):
    common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
    models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
    # logins
    uid = common.authenticate(db, username, password, {})
    with open(dir1, "rb") as imageFile:
        ImageBase64 = base64.b64encode(imageFile.read())
        str = ImageBase64.decode('ascii')
    with open(dir2, "rb") as imageFile:
        ImageBase64 = base64.b64encode(imageFile.read())
        str1 = ImageBase64.decode('ascii')
    with open(dir3, "rb") as imageFile:
        ImageBase64 = base64.b64encode(imageFile.read())
        str2 = ImageBase64.decode('ascii')

    date_act = datetime.datetime.now()
    new_partner = {
        'name': "Viseu",  # adapt for local
        'reason': reason_for,
        'status': 1,
        'date': date_act,
        'notes': "Problem! Slide or Lock",
    }

    new_contact = models.execute_kw(db, uid, password, 'open.cvrail', 'create', [new_partner])
    new_image = {
        'n_image': 1,
        'photo': str,
        'image_id': new_contact,
    }

    new_image1 = {
        'n_image': 2,
        'photo': str1,
        'image_id': new_contact,
    }
    new_image2 = {
        'n_image': 3,
        'photo': str2,
        'image_id': new_contact,
    }

    new_image_id = models.execute_kw(db, uid, password, 'open.cvrail.image', 'create', [new_image])
    new_image_id1 = models.execute_kw(db, uid, password, 'open.cvrail.image', 'create', [new_image1])
    new_image_id1 = models.execute_kw(db, uid, password, 'open.cvrail.image', 'create', [new_image2])
    os.remove(dir3)
    os.remove("/home/simao/dev/cvrail/tests/image_{}.txt".format(num_txt))
    if reason_for == 'Lack of slopes':
        os.remove("/home/simao/dev/cvrail/static/img/image_{}.png".format(num_txt - 1))
        os.remove("/home/simao/dev/cvrail/tests/image_{}.txt".format(num_txt - 1))
    print("New created id is", new_contact)
