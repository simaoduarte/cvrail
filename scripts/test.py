import numpy as np


def diff(new_image, original_image, number_image):
    i = 0
    for i in range(0, len(new_image)):
        if new_image[i] != original_image[i]:
            x = np.subtract(int(new_image[i]), int(original_image[i]))
            if abs(x) > 50:
                print("Error! Slide. Image->", number_image)
                return 0
    return 1


def diff_corners(new_image, number_image, original_image):
    difference = new_image - original_image
    if difference >= 0:
        print("There are {} more slopes! Image->".format(difference), number_image)
    else:
        print("Missing -> {} slopes! Image->".format(abs(difference)), number_image)
    return new_image
