import re
import sys
import test
import numpy as np
import script_odoo
import unittest
import cv2
import cv2.aruco as aruco

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters = aruco.DetectorParameters_create()
num = 0
path = "/home/simao/dev/cvrail/"


class compare(unittest.TestCase):
    def test_compare(self):
        num_image = 0
        fault = 0
        while num_image is not None:
            image = cv2.imread(path + "static/img/image_{}.png".format(num_image), 1)
            if image is None:
                break
            else:
                num_image += 1
        dir3 = path + "static/img/image_{}.png".format(num)
        for i in range(0, num_image):
            with self.subTest('Passed'):
                dir1 = path + "static/img/image_{}.png".format(num)
                img2 = cv2.imread(dir1, 1)
                cor, id, rejectedImgPoints = aruco.detectMarkers(img2, aruco_dict, parameters=parameters)
                out = aruco.drawDetectedMarkers(img2, cor, id)
                dir2 = path + "static/img/image_{}.png".format(i)
                img = cv2.imread(dir2, 1)
                corners, ids, rejectedImgPoints = aruco.detectMarkers(img, aruco_dict, parameters=parameters)
                out = aruco.drawDetectedMarkers(img, corners, ids)
                with open(path + "tests/image_{}.txt".format(num), 'r') as file:
                    data = file.read()
                    y = re.findall(r"[-+]?\d*\.\d+|\d+", data)
                if len(corners) == len(cor):
                    with open(path + "tests/image_{}.txt".format(i), 'r') as f:
                        fi = f.read()
                        x = re.findall(r"[-+]?\d*\.\d+|\d+", fi)
                        if test.diff(x, y, i) == 1:
                            self.assertEqual(test.diff(x, y, i), 1)
                            dir3 = path + "static/img/image_{}.png".format(i)
                        else:
                            y = x
                            reason = 'Slide'
                            script_odoo.script(dir1, dir3, dir2, reason, i)
                            self.assertEqual(test.diff(x, y, i), 1)
                        y = 1
                else:
                    corners = cor
                    if fault == 1:
                        reason = 'Lack of slopes'
                        script_odoo.script(dir1, dir3, dir2, reason, i)
                        fault = 0
                    fault = 1
                    self.assertEqual(test.diff_corners(len(corners), i, len(cor)), len(cor))


def test_main(out=sys.stderr, verbosity=2):
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromModule(sys.modules[__name__])
    unittest.TextTestRunner(out, verbosity=verbosity).run(suite)


if __name__ == '__main__':
    path = "/home/simao/dev/cvrail/"
    with open(path + "tests/test.txt", 'w') as f:
        test_main(f)
