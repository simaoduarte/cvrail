# Cvrail
**(PromptEquation company project)**
 ## Summary

   This work aims to detect the movement of the Aruco squares.
   This way, you can prevent slope movements and prevent rail accidents.

Follow me:
	
*	[Linkedin](https://www.linkedin.com/in/sim%C3%A3o-duarte-161652199/)

Follow company:
	
*	[Website](https://www.odoogap.com/)
	
*	[Linkedin](https://www.linkedin.com/company/odoogap/?originalSubdomain=pt)

## Installation:

   First install requirements.txt with the command:
   
   ```bash
    $ pip install -r requirements.txt
```

   For install Opencv and Aruco i use this script:
   
   *	[Script](https://gitlab.com/simaoduarte/scripts/-/blob/master/install-opencv-aruco.sh) 
   
   You need to change path in python files and scripts.
    
   Now it will be necessary for some python files to run in Crontab so that Raspberry Pi is completely autonomous. For such:
   
   ```bash
    $ crontab -e
```
   In crontab:
   
   ```bash
    # m h  dom mon dow   command
    #
    0 * * * * /home/simao/dev/cvrail/scripts/photo.sh
    1 * * * * python3 /home/simao/dev/cvrail/cvrail/savearray.py
    4 * * * * python3 /home/simao/dev/cvrail/scripts/comparation.py
    * /4 * * * python3 /home/simao/dev/cvrail/scripts/keepalive.py
    0 23 8,25 * * python3 /home/simao/dev/cvrail/scripts/reset.py

```